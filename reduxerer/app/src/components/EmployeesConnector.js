import {connect} from 'react-redux';
import Employees from './Employees';
import {addEmployee, filterByDepartment} from './../actions';

const getFilteredEmployees = (state) => {
    if (state.activeDepartment === 'all') {
        return state.employees;
    }

    return state.employees.filter(e =>
        e.position === state.activeDepartment
    );
};

const mapStateToProps = (state) => ({
    employees: getFilteredEmployees(state),
    activeDepartment: state.activeDepartment,
});

const mapDispatchToProps = (dispatch) => ({
    onClick: (name, position) => {
        dispatch(addEmployee(name, position));
    },
    onFilter: (filter) => {
        dispatch(filterByDepartment(filter));
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Employees);
