import {describe, it} from 'mocha';
import {expect} from 'chai';
import {filterByDepartment} from './actions';

describe('actions test', () => {
    it('should create an action to receive employees', () => {
        const department = 'dev';
        const expectedAction = {
            type: 'FILTER_BY_DEPARTMENT',
            department,
        };
        expect(filterByDepartment(department)).to.eql(expectedAction);
    });
});
