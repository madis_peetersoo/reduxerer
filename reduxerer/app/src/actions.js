import api from './api';

export const addEmployee = (name, position) => ({
    type: 'ADD_EMPLOYEE',
    name,
    position,
});

export const filterByDepartment = (department) => ({
    type: 'FILTER_BY_DEPARTMENT',
    department,
});


/*
    Because you usually might want to dispatch multiple actions in a async action creator
    I've included the thunk middleware in our store and implemented an action creator that
    return a function
 */
export const fetchEmployees = () => (dispatch) => {
    const employeesList = api.employeesList;
    // dispatch(filterByDepartment('dev'));

    return employeesList.fetch().then(response => {
        dispatch({
            type: 'RECEIVE_EMPLOYEES',
            response,
        });
    }).catch(() => {
        // Do something nice with the error
    });
};
