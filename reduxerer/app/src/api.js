import Router, {Resource} from 'tg-resources';

// The DJ_CONST is undefined when running npm tests
const employeesListUrl = typeof DJ_CONST !== 'undefined' ? DJ_CONST.employeesList : 'api/employees/';

const api = new Router({
    employeesList: new Resource(employeesListUrl),
}, {
    headers: () => ({
        Accept: 'application/json',
    }),
});

export default api;
