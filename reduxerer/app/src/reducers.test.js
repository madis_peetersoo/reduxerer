import {describe, it} from 'mocha';
import {expect} from 'chai';
import {appReducer} from './reducers';

const initialState = {
    employees: [],
    activeDepartment: 'all',
};

describe('reducer tests', () => {
    it('should return the initial state', () => {
        expect(
            appReducer(undefined, {})
        ).to.eql(initialState);
    });

    it('should change the active department to dev', () => {
        const filterDepartment = 'dev';
        expect(
            appReducer({}, {
                type: 'FILTER_BY_DEPARTMENT',
                department: filterDepartment,
            })
        ).to.eql({...initialState, activeDepartment: filterDepartment});
    });
});
